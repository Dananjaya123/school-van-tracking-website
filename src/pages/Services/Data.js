export const homeObjOne = {
    lightBg: false,
    primary:true,
    imgStart: '',
    lightTopLine: true,
    lightTextDesc: true, 
    description: 'We make sure your child is safe when they travel to school and back home.', 
    headline: 'Welcom to School Van Tracking System', 
    lightText: true,
    topLine: '',
    img: require('../../images/image1.jpg'), 
    alt: 'Image', 
    start: '',
}

export const homeObjTwo = {
    lightBg: true,
    primary: false,
    imgStart: 'start',
    lightTopLine: false,
    lightTextDesc: false, 
    description: 'We provide real time location tracking for your child so that you can know they are safe.', 
    headline: 'Live Tracking With GPS', 
    lightText: false,
    topLine: '',
    img: require('../../images/image2.jpg'), 
    alt: 'Image', 
    start: '',
}

export const homeObjThree = {
    lightBg: false,
    primary:true,
    imgStart: '',
    lightTopLine: true,
    lightTextDesc: true, 
    description: 'Live location updates through smart watch GPS to your smartphone.', 
    headline: 'Smart Watch for your child', 
    lightText: true,
    topLine: '',
    img: require('../../images/image3.jpg'), 
    alt: 'Image', 
    start: '',
}

export const homeObjFour = {
    lightBg: true,
    primary: false,
    imgStart: 'start',
    lightTopLine: false,
    lightTextDesc: false, 
    description: 'We made it easy for driver and parent to update their status and communicate directly.', 
    headline: 'Driver App and Parent App', 
    lightText: false,
    topLine: '',
    img: require('../../images/image4.jpg'), 
    alt: 'Image', 
    start: '',
}