import React from 'react'
import * as FaIcons from "react-icons/fa"
import * as AiIcons from 'react-icons/ai'
import * as IoIcons from 'react-icons/io'


export const SidebarData = [
    {
        title: 'Home',
        path: '/adminhome',
        icon: <AiIcons.AiFillHome/>,
        cName: 'nav-text',
    },
    {
        title: 'Users',
        path: '/users',
        icon: <IoIcons.IoMdPeople/>,
        cName: 'nav-text',
    },
    {
        title: 'Messages',
        path: '/messages',
        icon: <IoIcons.IoMdPaperPlane/>,
        cName: 'nav-text',
    },
    {
        title: 'Van Fare',
        path: '/vanfare',
        icon: <AiIcons.AiFillWallet/>,
        cName: 'nav-text',
    },
    {
        title: 'Settings',
        path: '/settings',
        icon: <AiIcons.AiFillSetting/>,
        cName: 'nav-text',
    },
    {
        title: 'Reports',
        path: '/reports',
        icon: <IoIcons.IoIosPaper/>,
        cName: 'nav-text',
    },
]
