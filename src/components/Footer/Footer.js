import React from "react";
import { FaFacebook, FaInstagram, FaLinkedin, FaYoutube } from "react-icons/fa";
import {
  FooterContainer,
  FooterSubscription,
  FooterSubHeading,
  FooterSubText,
  Form,
  FormInput,
  FooterLinksContainer,
  FooterLinksWrapper,
  FooterLinksItems,
  FooterLinkTitle,
  FooterLink,
  SocialMedia,
  SocialMediaWrap,
  SocialLogo,
  WebsiteRights,
  SocialIcon,
  SocialIcons,
  SocialIconLink
} from "./Footer_elements";

const Footer = () => {
  return (
    <>
      <FooterContainer>
        <FooterSubscription>
          <FooterSubHeading>
            Join our services and keep your child safe while travelling.
          </FooterSubHeading>
          <FooterSubText>You can unsubscribe at any time.</FooterSubText>
          <Form>
            <FormInput
              name="email"
              type="email"
              placeholder="Your Email"
            />
          </Form>
          <FooterSubscription>
              <FooterLinksContainer>
                  <FooterLinksWrapper>
                      <FooterLinksItems>
                          <FooterLinkTitle>About Us</FooterLinkTitle>
                          <FooterLink to='/'>How It Works</FooterLink>
                          <FooterLink to='/'>Testimonials</FooterLink>
                          <FooterLink to='/'>Terms of Services</FooterLink>
                      </FooterLinksItems>
                      <FooterLinksItems>
                          <FooterLinkTitle>Contact Us</FooterLinkTitle>
                          <FooterLink to='/'>Contact</FooterLink>
                          <FooterLink to='/'>Support</FooterLink>
                          <FooterLink to='/'>Destinations</FooterLink>
                      </FooterLinksItems>
                      </FooterLinksWrapper>
                      <FooterLinksWrapper>
                      <FooterLinksItems>
                          <FooterLinkTitle>Social Media</FooterLinkTitle>
                          <FooterLink to='/'>Instagram</FooterLink>
                          <FooterLink to='/'>Facebook</FooterLink>
                          <FooterLink to='/'>Youtube</FooterLink>
                          <FooterLink to='/'>Linkedin</FooterLink>
                      </FooterLinksItems>
                  </FooterLinksWrapper>
              </FooterLinksContainer>
          </FooterSubscription>
        </FooterSubscription>
        <SocialMedia>
            <SocialMediaWrap>
                <SocialLogo>
                    <SocialIcon/>
                    School Van Tracking
                </SocialLogo>
                <WebsiteRights>
                    School Van Tracking © 2020
                </WebsiteRights>
                <SocialIcons>
                    <SocialIconLink href='/' target="_blank" aria-label="Facebook">
                        <FaFacebook/>
                    </SocialIconLink>
                    <SocialIconLink href='/' target="_blank" aria-label="Instagram">
                        <FaInstagram/>
                    </SocialIconLink>
                    <SocialIconLink href='/' target="_blank" aria-label="Youtube">
                        <FaYoutube/>
                    </SocialIconLink>
                    <SocialIconLink href='/' target="_blank" aria-label="Linkedin">
                        <FaLinkedin/>
                    </SocialIconLink>
                </SocialIcons>
            </SocialMediaWrap>
        </SocialMedia>
      </FooterContainer>
    </>
  );
};

export default Footer;
