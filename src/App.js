import React from 'react';
import GlobalStyles from './globalStyles';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { Navbar, Footer } from './components';
import Home from './pages/HomePage/Home';
import Services from './pages/Services/Services';
import ScrollToTop from './components/ScrollToTop';
import AdminNavbar from './components/AdminNavbar/AdminNavbar';
import FormSignup from './components/Login/FormSignup';

function App() {
  return (
    <Router>
      <GlobalStyles/>
      <ScrollToTop/>
      <Navbar/>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/services" exact component={Services}/>
        <Route path="/signup" exact component={FormSignup}/>
        {/* <Route exact path="/admin" exact component={AdminNavbar}/> */}
      </Switch>
      <Footer/>
    </Router>
  );
}

export default App;
