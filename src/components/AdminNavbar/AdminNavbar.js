import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import * as FaIcons from "react-icons/fa"
import * as AiIcons from 'react-icons/ai'
import {SidebarData} from './SidebarData'
import './Navbar.css'
import {IconContext} from 'react-icons'

import { BrowserRouter as Router, Route } from "react-router-dom";
import AdminHome from '../../pages/Admin/AdminHome'
import Users from '../../pages/Admin/Users'
import Messages from '../../pages/Admin/Messages'
import VanFare from '../../pages/Admin/VanFare'
import Settings from '../../pages/Admin/Settings'
import Reports from '../../pages/Admin/Reports'




function AdminNavbar() {
    const [sidebar, setSidebar] = useState(false);

    const showSidebar = () => setSidebar(!sidebar);  
    return (
        
        <Router>
        <Route render={({ location, history }) => (
        <>
        <IconContext.Provider value={{color: '#fff'}}>
            <div className="navbar">
                <Link to="#" className='menu-bars'>
                    <FaIcons.FaBars onClick={showSidebar}/>
                </Link>
            </div>
            <nav className={sidebar ? 'nav-menu-active' : 'nav-menu'} onSelect={(selected) => {
                                const to = '/' + selected;
                                if (location.pathname !== to) {
                                    history.push(to);
                                }
                            }}>
                <ul className='nav-menu-items' onClick={showSidebar}>
                    <li className='navbar-toggle'>
                        <Link to="#" className='menu-bars'>
                            <AiIcons.AiOutlineClose/>
                        </Link>
                    </li>
                    {SidebarData.map((item, index) => {
                        return (
                            <li key={index} className={item.cName}>
                                <Link to={item.path}>
                                    {item.icon}
                                    <span>{item.title}</span>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
            </IconContext.Provider>
            <main>
                <Route path="/admin" exact component={AdminNavbar} />
                <Route path="/adminhome" exact component={AdminHome} />
                <Route path="/users" exact component={Users} />
                <Route path="/messages" exact component={Messages} />
                <Route path="/vanfare" exact component={VanFare} />
                <Route path="/settings" exact component={Settings} />
                <Route path="/reports" exact component={Reports} />
            </main>
            </>
              )}/>
            </Router>
        
    )
}

export default AdminNavbar
